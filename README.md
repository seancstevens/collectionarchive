# README #

### What is this repository for? ###

* Collection Archive - Back End
* Built using .NET, Entity Framework Core, Fluent API, SQL Server
* Version 1
* https://seancstevens@bitbucket.org/seancstevens/collectionarchive.git

### How do I get set up? ###

* Open solution in Visual Studio.
* Open Package Manager Console, select the DAL class library and run Update-Database.
* A database will be created in the local db.
* Set startup project as CA.Seed and run the program.
* The database will be populated with seed data.
* Set startup project as CA.API

### Contribution guidelines ###

* Any code review and feedback would be appreciated.

### Who do I talk to? ###

* Sean Stevens
* sean.stevens@stgconsulting.com
