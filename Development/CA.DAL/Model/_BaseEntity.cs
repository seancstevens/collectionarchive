﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CA.DAL.Model
{
    public abstract partial class BaseEntity : IBaseEntity
    {
        public virtual long Id { get; set; }
        [JsonIgnore]
        public virtual DateTime? ModifiedDate { get; set; }
        [JsonIgnore]
        public virtual DateTime StartDate { get; set; }
        [JsonIgnore]
        public virtual DateTime? EndDate { get; set; }
        [JsonIgnore]
        public virtual DateTime CreatedDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [JsonIgnore]
        public bool IsActive
        {
            get
            {
                return StartDate <= DateTime.Now &&
                    (!EndDate.HasValue || EndDate > DateTime.Now);
            }
            private set { }
        }
    }

    public interface IBaseEntity
    {
        long Id { get; set; }
        DateTime? ModifiedDate { get; set; }
        DateTime StartDate { get; set; }
        DateTime? EndDate { get; set; }
        DateTime CreatedDate { get; set; }
        bool IsActive { get; }
    }
}
