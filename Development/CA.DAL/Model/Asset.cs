﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class Asset : BaseEntity
    {
        // Fields
        public string Name { get; set; }


        // Foreign Key Relationships
        public long AssetCollectionId { get; set; }
        public virtual AssetCollection AssetCollection { get; set; }

        public long AssetTypeId { get; set; }
        public virtual AssetType AssetType { get; set; }


        // One-To-Many Relationships
        public virtual List<Image> Images { get; set; }
        public virtual List<TextFieldValue> TextFieldValues { get; set; }
        public virtual List<CategorizationValue> CategorizationValues { get; set; }
    }
}
