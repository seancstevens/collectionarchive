﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CA.DAL.Model.NonDb
{
    [NotMapped]
    public class UserSearch : BaseSearch
    {
        public string Username { get; set; }
    }
}
