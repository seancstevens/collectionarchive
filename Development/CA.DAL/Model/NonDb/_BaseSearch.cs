﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CA.DAL.Model.NonDb
{
    [NotMapped]
    public abstract class BaseSearch
    {
        public int? StartRecord { get; set; }
        public int? Count { get; set; }
        public string Sort { get; set; }
        public bool ReverseSort { get; set; }
        public bool IncludeDeleted { get; set; }
    }
}
