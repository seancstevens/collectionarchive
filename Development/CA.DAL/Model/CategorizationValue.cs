﻿namespace CA.DAL.Model
{
    public class CategorizationValue : BaseEntity
    {
        // Foreign Key Relationships
        public long AssetId { get; set; }
        public virtual Asset Asset { get; set; }

        public long CategorizationId { get; set; }
        public virtual Categorization Categorization { get; set; }

        public long CategorizationOptionId { get; set; }
        public virtual CategorizationOption CategorizationOption { get; set; }

    }
}
