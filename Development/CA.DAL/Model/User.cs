﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class User : BaseEntity
    {
        // Fields
        public string Username { get; set; }
        public string Password { get; set; }


        // One-To-Many Relationships
        public virtual List<AssetCollection> AssetCollections { get; set; }
    }
}
