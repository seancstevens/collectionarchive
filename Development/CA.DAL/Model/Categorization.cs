﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class Categorization : BaseEntity
    {
        // Fields
        public string Name { get; set; }
        public int? SortOrder { get; set; }


        // Foreign Key Relationships
        public long AssetCollectionId { get; set; }
        public virtual AssetCollection AssetCollection { get; set; }


        // One-To-Many Relationships
        public virtual List<CategorizationOption> CategorizationOptions { get; set; }
        public virtual List<CategorizationValue> CategorizationValues { get; set; }
    }
}
