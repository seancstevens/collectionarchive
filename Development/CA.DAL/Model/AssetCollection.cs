﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class AssetCollection : BaseEntity
    {
        // Fields
        public string Name { get; set; }
        public int? SortOrder { get; set; }


        // Foreign Key Relationships
        public long UserId { get; set; }
        public virtual User User { get; set; }


        // One-To-Many Relationships
        public virtual List<Asset> Assets { get; set; }
        public virtual List<TextField> TextFields { get; set; }
        public virtual List<Categorization> Categorizations { get; set; }
    }
}
