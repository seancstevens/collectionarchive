﻿namespace CA.DAL.Model
{
    public class Image : BaseEntity
    {
        // Fields
        public string Name { get; set; }
        public int? SortOrder { get; set; }
        public byte[] ImageData { get; set; }


        // Foreign Key Relationships
        public long AssetId { get; set; }
        public virtual Asset Asset { get; set; }
    }
}
