﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class CategorizationOption : BaseEntity
    {
        // Fields
        public string Name { get; set; }


        // Foreign Key Relationships
        public long CategorizationId { get; set; }
        public virtual Categorization Categorization { get; set; }


        // One-To-Many Relationships
        public virtual List<CategorizationValue> CategorizationValues { get; set; }
    }
}
