﻿namespace CA.DAL.Model
{
    public class TextFieldValue : BaseEntity
    {
        // Fields
        public string FieldValue { get; set; }


        // Foreign Key Relationships
        public long AssetId { get; set; }
        public virtual Asset Asset { get; set; }

        public long TextFieldId { get; set; }
        public virtual TextField TextField { get; set; }
    }
}
