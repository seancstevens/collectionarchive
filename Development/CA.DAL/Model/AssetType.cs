﻿using System.Collections.Generic;

namespace CA.DAL.Model
{
    public class AssetType : BaseEntity
    {
        // Fields
        public string Name { get; set; }


        // One-To-Many Relationships
        public virtual List<Asset> Assets { get; set; }
    }
}
