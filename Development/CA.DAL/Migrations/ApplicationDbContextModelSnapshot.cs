﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CA.DAL;

namespace CA.DAL.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CA.DAL.Model.Asset", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetCollectionId");

                    b.Property<long>("AssetTypeId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("AssetCollectionId");

                    b.HasIndex("AssetTypeId");

                    b.ToTable("Asset");
                });

            modelBuilder.Entity("CA.DAL.Model.AssetCollection", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<int?>("SortOrder");

                    b.Property<DateTime>("StartDate");

                    b.Property<long>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AssetCollection");
                });

            modelBuilder.Entity("CA.DAL.Model.AssetType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.ToTable("AssetType");
                });

            modelBuilder.Entity("CA.DAL.Model.Categorization", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetCollectionId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<int?>("SortOrder");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("AssetCollectionId");

                    b.ToTable("Categorization");
                });

            modelBuilder.Entity("CA.DAL.Model.CategorizationOption", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CategorizationId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("CategorizationId");

                    b.ToTable("CategorizationOption");
                });

            modelBuilder.Entity("CA.DAL.Model.CategorizationValue", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetId");

                    b.Property<long>("CategorizationId");

                    b.Property<long>("CategorizationOptionId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("AssetId");

                    b.HasIndex("CategorizationId");

                    b.HasIndex("CategorizationOptionId");

                    b.ToTable("CategorizationValue");
                });

            modelBuilder.Entity("CA.DAL.Model.Image", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<byte[]>("ImageData");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<int?>("SortOrder");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("AssetId");

                    b.ToTable("Image");
                });

            modelBuilder.Entity("CA.DAL.Model.TextField", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetCollectionId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<int?>("SortOrder");

                    b.Property<DateTime>("StartDate");

                    b.HasKey("Id");

                    b.HasIndex("AssetCollectionId");

                    b.ToTable("TextField");
                });

            modelBuilder.Entity("CA.DAL.Model.TextFieldValue", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("AssetId");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<string>("FieldValue");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<DateTime>("StartDate");

                    b.Property<long>("TextFieldId");

                    b.HasKey("Id");

                    b.HasIndex("AssetId");

                    b.HasIndex("TextFieldId");

                    b.ToTable("TextFieldValue");
                });

            modelBuilder.Entity("CA.DAL.Model.User", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Password");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.ToTable("User");
                });

            modelBuilder.Entity("CA.DAL.Model.Asset", b =>
                {
                    b.HasOne("CA.DAL.Model.AssetCollection", "AssetCollection")
                        .WithMany("Assets")
                        .HasForeignKey("AssetCollectionId");

                    b.HasOne("CA.DAL.Model.AssetType", "AssetType")
                        .WithMany("Assets")
                        .HasForeignKey("AssetTypeId");
                });

            modelBuilder.Entity("CA.DAL.Model.AssetCollection", b =>
                {
                    b.HasOne("CA.DAL.Model.User", "User")
                        .WithMany("AssetCollections")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("CA.DAL.Model.Categorization", b =>
                {
                    b.HasOne("CA.DAL.Model.AssetCollection", "AssetCollection")
                        .WithMany("Categorizations")
                        .HasForeignKey("AssetCollectionId");
                });

            modelBuilder.Entity("CA.DAL.Model.CategorizationOption", b =>
                {
                    b.HasOne("CA.DAL.Model.Categorization", "Categorization")
                        .WithMany("CategorizationOptions")
                        .HasForeignKey("CategorizationId");
                });

            modelBuilder.Entity("CA.DAL.Model.CategorizationValue", b =>
                {
                    b.HasOne("CA.DAL.Model.Asset", "Asset")
                        .WithMany("CategorizationValues")
                        .HasForeignKey("AssetId");

                    b.HasOne("CA.DAL.Model.Categorization", "Categorization")
                        .WithMany("CategorizationValues")
                        .HasForeignKey("CategorizationId");

                    b.HasOne("CA.DAL.Model.CategorizationOption", "CategorizationOption")
                        .WithMany("CategorizationValues")
                        .HasForeignKey("CategorizationOptionId");
                });

            modelBuilder.Entity("CA.DAL.Model.Image", b =>
                {
                    b.HasOne("CA.DAL.Model.Asset", "Asset")
                        .WithMany("Images")
                        .HasForeignKey("AssetId");
                });

            modelBuilder.Entity("CA.DAL.Model.TextField", b =>
                {
                    b.HasOne("CA.DAL.Model.AssetCollection", "AssetCollection")
                        .WithMany("TextFields")
                        .HasForeignKey("AssetCollectionId");
                });

            modelBuilder.Entity("CA.DAL.Model.TextFieldValue", b =>
                {
                    b.HasOne("CA.DAL.Model.Asset", "Asset")
                        .WithMany("TextFieldValues")
                        .HasForeignKey("AssetId");

                    b.HasOne("CA.DAL.Model.TextField", "TextField")
                        .WithMany("TextFieldValues")
                        .HasForeignKey("TextFieldId");
                });
        }
    }
}
