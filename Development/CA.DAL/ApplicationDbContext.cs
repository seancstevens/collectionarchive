﻿using CA.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CA.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<AssetCollection> AssetCollections { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetType> AssetTypes { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<TextField> TextFields { get; set; }
        public DbSet<TextFieldValue> TextFieldValues { get; set; }
        public DbSet<Categorization> Categorizations { get; set; }
        public DbSet<CategorizationOption> CategorizationOptions { get; set; }
        public DbSet<CategorizationValue> CategorizationValues { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["CA_Connection"].ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // FLUENT API Relationships - One-to-Many
            modelBuilder.Entity<AssetCollection>()
                .HasOne(e => e.User)
                .WithMany(e => e.AssetCollections)
                .HasForeignKey(e => e.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<Asset>()
                .HasOne(e => e.AssetCollection)
                .WithMany(e => e.Assets)
                .HasForeignKey(e => e.AssetCollectionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<Asset>()
                .HasOne(e => e.AssetType)
                .WithMany(e => e.Assets)
                .HasForeignKey(e => e.AssetTypeId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<Image>()
                .HasOne(e => e.Asset)
                .WithMany(e => e.Images)
                .HasForeignKey(e => e.AssetId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<TextField>()
                .HasOne(e => e.AssetCollection)
                .WithMany(e => e.TextFields)
                .HasForeignKey(e => e.AssetCollectionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<TextFieldValue>()
                .HasOne(e => e.TextField)
                .WithMany(e => e.TextFieldValues)
                .HasForeignKey(e => e.TextFieldId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<TextFieldValue>()
                .HasOne(e => e.Asset)
                .WithMany(e => e.TextFieldValues)
                .HasForeignKey(e => e.AssetId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<Categorization>()
                .HasOne(e => e.AssetCollection)
                .WithMany(e => e.Categorizations)
                .HasForeignKey(e => e.AssetCollectionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<CategorizationOption>()
                .HasOne(e => e.Categorization)
                .WithMany(e => e.CategorizationOptions)
                .HasForeignKey(e => e.CategorizationId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<CategorizationValue>()
                .HasOne(e => e.Asset)
                .WithMany(e => e.CategorizationValues)
                .HasForeignKey(e => e.AssetId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<CategorizationValue>()
                .HasOne(e => e.Categorization)
                .WithMany(e => e.CategorizationValues)
                .HasForeignKey(e => e.CategorizationId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            modelBuilder.Entity<CategorizationValue>()
                .HasOne(e => e.CategorizationOption)
                .WithMany(e => e.CategorizationValues)
                .HasForeignKey(e => e.CategorizationOptionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            

            // REMOVE PLURALIZING
            modelBuilder.RemovePluralizingTableNameConvention();
        }
    }

    public static class ModelBuilderExtensions
    {
        public static void RemovePluralizingTableNameConvention(this ModelBuilder modelBuilder)
        {
            foreach (IMutableEntityType entity in modelBuilder.Model.GetEntityTypes())
            {
                entity.Relational().TableName = entity.DisplayName();
            }
        }
    }
}
