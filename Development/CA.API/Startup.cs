﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CA.API.Startup))]

namespace CA.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
