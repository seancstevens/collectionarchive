﻿using CA.DAL;
using CA.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace CA.API.Controllers
{
    public abstract class BaseController<TEntity> : ApiController where TEntity : class, IBaseEntity
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        private readonly DbSet<TEntity> dbSet;
        
        protected BaseController(Func<ApplicationDbContext, DbSet<TEntity>> getDbSet)
        {
            dbSet = getDbSet(db);
        }
        
        public IHttpActionResult Get()
        {
            try
            {
                var results = dbSet.Where(item => item.IsActive == true);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }

        public async Task<IHttpActionResult> GetById(long id)
        {
            try
            {
                var results = dbSet.Where(item => item.Id == id);
                TEntity retVal = await results.FirstOrDefaultAsync();

                if (retVal == null)
                {
                    return NotFound();
                }

                return Ok(retVal);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }

        public async Task<IHttpActionResult> Post(TEntity entity)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                entity.CreatedDate = DateTime.Now;
                entity.StartDate = DateTime.Now;

                db.Entry(entity).State = EntityState.Added;

                await db.SaveChangesAsync();

                return Created($"api/{entity.GetType()}/{entity.Id}", entity);
            }
            catch (DbUpdateException)
            {
                return Conflict();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }

        public async Task<IHttpActionResult> Put(TEntity entity)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                entity.ModifiedDate = DateTime.Now;

                db.Entry(entity).State = EntityState.Modified;

                await db.SaveChangesAsync();

                return Ok(entity);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (DbUpdateException)
            {
                return Conflict();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }

        public async Task<IHttpActionResult> Delete(TEntity entity)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var results = dbSet.Where(item => item.Id == entity.Id);
                TEntity existingEntity = await results.FirstOrDefaultAsync();

                if (existingEntity == null)
                {
                    return NotFound();
                }

                existingEntity.EndDate = DateTime.Now;
                existingEntity.ModifiedDate = DateTime.Now;

                db.Entry(existingEntity).State = EntityState.Modified;

                await db.SaveChangesAsync();

                return Ok(existingEntity);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }
        
        public async Task<IHttpActionResult> Patch(TEntity entity)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var results = dbSet.Where(item => item.Id == entity.Id);
                TEntity existingEntity = await results.FirstOrDefaultAsync();

                if (existingEntity == null)
                {
                    return NotFound();
                }

                existingEntity.EndDate = null;
                existingEntity.ModifiedDate = DateTime.Now;

                db.Entry(existingEntity).State = EntityState.Modified;

                await db.SaveChangesAsync();

                return Ok(existingEntity);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message + ex.InnerException);
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}