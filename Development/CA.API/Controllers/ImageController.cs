﻿using CA.DAL.Model;

namespace CA.API.Controllers
{
    public class ImageController : BaseController<Image>
    {
        public ImageController() : base(db => db.Images)
        {
        }
    }
}