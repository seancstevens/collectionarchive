﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class AssetTypeController : BaseController<AssetType>
    {
        internal AssetTypeLogic logic;

        public AssetTypeController() : base(db => db.AssetTypes)
        {
            logic = new AssetTypeLogic();
        }

        [HttpGet()]
        [Route("api/AssetType/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }
    }
}