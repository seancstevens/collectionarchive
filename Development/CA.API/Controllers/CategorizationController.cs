﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class CategorizationController : BaseController<Categorization>
    {
        internal CategorizationLogic logic;

        public CategorizationController() : base(db => db.Categorizations)
        {
            logic = new CategorizationLogic();
        }

        [HttpGet()]
        [Route("api/Categorization/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }

        [HttpGet()]
        [Route("api/Categorization/GetByCollectionId")]
        public IHttpActionResult GetByCollectionId(int id)
        {
            return Ok(logic.GetByCollectionId(id));
        }
    }
}