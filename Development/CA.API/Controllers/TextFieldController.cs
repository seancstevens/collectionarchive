﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class TextFieldController : BaseController<TextField>
    {
        internal TextFieldLogic logic;

        public TextFieldController() : base(db => db.TextFields)
        {
            logic = new TextFieldLogic();
        }

        [HttpGet()]
        [Route("api/TextField/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }

        [HttpGet()]
        [Route("api/TextField/GetByCollectionId")]
        public IHttpActionResult GetByCollectionId(int id)
        {
            return Ok(logic.GetByCollectionId(id));
        }
    }
}