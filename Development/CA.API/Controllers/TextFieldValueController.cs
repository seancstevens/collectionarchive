﻿using CA.DAL.Model;

namespace CA.API.Controllers
{
    public class TextFieldValueController : BaseController<TextFieldValue>
    {
        public TextFieldValueController() : base(db => db.TextFieldValues)
        {
        }
    }
}