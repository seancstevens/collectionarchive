﻿using CA.DAL.Model;

namespace CA.API.Controllers
{
    public class CategorizationOptionController : BaseController<CategorizationOption>
    {
        public CategorizationOptionController() : base(db => db.CategorizationOptions)
        {
        }
    }
}