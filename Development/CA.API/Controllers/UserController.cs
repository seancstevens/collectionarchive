﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class UserController : BaseController<User>
    {
        internal UserLogic logic;

        public UserController() : base(db => db.Users)
        {
            logic = new UserLogic();
        }

        [HttpGet()]
        [Route("api/User/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }

        [HttpGet()]
        [Route("api/User/GetByUsername")]
        public IHttpActionResult GetByUsername(string userName)
        {
            return Ok(logic.GetByUsername(userName));
        }
    }
}