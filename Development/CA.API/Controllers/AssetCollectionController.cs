﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class AssetCollectionController : BaseController<AssetCollection>
    {
        internal AssetCollectionLogic logic;

        public AssetCollectionController() : base(db => db.AssetCollections)
        {
            logic = new AssetCollectionLogic();
        }

        [HttpGet()]
        [Route("api/AssetCollection/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }

        [HttpGet()]
        [Route("api/AssetCollection/GetDisabledByUserId")]
        public IHttpActionResult GetDisabledByUserId(int userId)
        {
            return Ok(logic.GetDisabledByUserId(userId));
        }

        [HttpGet()]
        [Route("api/AssetCollection/GetByUserId")]
        public IHttpActionResult GetByUserId(int userId)
        {
            return Ok(logic.GetByUserId(userId));
        }

        [HttpGet()]
        [Route("api/AssetCollection/GetCollectionDetail")]
        public IHttpActionResult GetCollectionDetail(int id)
        {
            return Ok(logic.GetCollectionDetail(id));
        }

        [HttpPost()]
        [Route("api/AssetCollection/BulkPost")]
        public IHttpActionResult BulkPost(AssetCollection assetCollection)
        {
            return Ok(logic.BulkPost(assetCollection));
        }

        [HttpPut()]
        [Route("api/AssetCollection/BulkPut")]
        public IHttpActionResult BulkPut(AssetCollection assetCollection)
        {
            return Ok(logic.BulkPut(assetCollection));
        }
    }
}