﻿using CA.DAL.Model;

namespace CA.API.Controllers
{
    public class CategorizationValueController : BaseController<CategorizationValue>
    {
        public CategorizationValueController() : base(db => db.CategorizationValues)
        {
        }
    }
}