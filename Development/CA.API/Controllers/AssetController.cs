﻿using CA.BLL;
using CA.DAL.Model;
using System.Web.Http;

namespace CA.API.Controllers
{
    public class AssetController : BaseController<Asset>
    {
        internal AssetLogic logic;

        public AssetController() : base(db => db.Assets)
        {
            logic = new AssetLogic();
        }

        [HttpGet()]
        [Route("api/Asset/GetDisabled")]
        public IHttpActionResult GetDisabled()
        {
            return Ok(logic.GetDisabled());
        }

        [HttpGet()]
        [Route("api/Asset/GetByCollectionId")]
        public IHttpActionResult GetByCollectionId(int id)
        {
            return Ok(logic.GetByCollectionId(id));
        }

        [HttpGet()]
        [Route("api/Asset/GetDisabledByCollectionId")]
        public IHttpActionResult GetDisabledByCollectionId(int id)
        {
            return Ok(logic.GetDisabledByCollectionId(id));
        }

        [HttpPost()]
        [Route("api/Asset/BulkPost")]
        public IHttpActionResult BulkPost(Asset asset)
        {
            return Ok(logic.BulkPost(asset));
        }

        [HttpPut()]
        [Route("api/Asset/BulkPut")]
        public IHttpActionResult BulkPut(Asset asset)
        {
            return Ok(logic.BulkPut(asset));
        }
    }
}