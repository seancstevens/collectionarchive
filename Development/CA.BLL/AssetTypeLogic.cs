﻿using CA.DAL.Model;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class AssetTypeLogic : BaseLogic
    {
        public IList<AssetType> GetDisabled()
        {
            var retVal = db.AssetTypes.Where(item => item.IsActive == false);
            return retVal.ToList();
        }
    }
}
