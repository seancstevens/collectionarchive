﻿using CA.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class AssetCollectionLogic : BaseLogic
    {
        public IList<AssetCollection> GetDisabled()
        {
            IQueryable<AssetCollection> retVal = db.AssetCollections.Where(item => item.IsActive == false);
            return retVal.ToList();
        }

        public IList<AssetCollection> GetDisabledByUserId(int userId)
        {
            IQueryable<AssetCollection> retVal = db.AssetCollections
                .Where(u => u.IsActive == false)
                .Where(u => u.UserId == userId);

            return retVal.ToList();
        }

        public IList<AssetCollection> GetByUserId(int userId)
        {
            IQueryable<AssetCollection> retVal = db.AssetCollections
                .Include(i => i.Categorizations)
                .Include(i => i.TextFields)
                .Where(u => u.IsActive == true)
                .Where(u => u.UserId == userId);

            return retVal.ToList();
        }

        public AssetCollection GetCollectionDetail(int id)
        {
            var assetCollection = db.AssetCollections
                .Include(i => i.User)
                .FirstOrDefault(i => i.Id == id);

            return assetCollection;
        }

        public AssetCollection BulkPost(AssetCollection assetCollection)
        {
            assetCollection.CreatedDate = DateTime.Now;
            assetCollection.StartDate = DateTime.Now;
            db.AssetCollections.Add(assetCollection);
            db.SaveChanges();

            return assetCollection;
        }

        public AssetCollection BulkPut(AssetCollection assetCollection)
        {
            assetCollection.ModifiedDate = DateTime.Now;
            db.AssetCollections.Update(assetCollection);
            db.SaveChanges();

            return assetCollection;
        }
    }
}
