﻿using CA.DAL.Model;
using CA.DAL.Model.NonDb;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class UserLogic : BaseLogic
    {
        public IList<User> GetDisabled()
        {
            IQueryable<User> retVal = db.Users.Where(item => item.IsActive == false);
            return retVal.ToList();
        }

        public IQueryable<User> GetByUsername(string userName)
        {
            var user = db.Users.Where(u => u.Username == userName);
            return user;
        }
        
        public IList<User> GetBySearch(UserSearch search)
        {
            IQueryable<User> retVal = db.Users.Where(u => u.IsActive == true);
            
            if (!string.IsNullOrWhiteSpace(search.Username))
            {
                retVal = retVal.Where(user => user.Username.Contains(search.Username));
            }

            retVal = AddSortAndPaging(search, retVal);

            return retVal.ToList();
        }
    }
}
