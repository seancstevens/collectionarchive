﻿using CA.DAL;
using CA.DAL.Model.NonDb;
using System.Linq;
using System.Linq.Dynamic;

namespace CA.BLL
{
    public abstract class BaseLogic
    {
        internal ApplicationDbContext db;

        public BaseLogic()
        {
            db = new ApplicationDbContext();
        }

        protected IQueryable<T> AddSortAndPaging<T>(BaseSearch search, IQueryable<T> query, string defaultSort = "Name")
        {
            search.Sort = search.Sort ?? defaultSort;

            query = query.OrderBy(string.Format("{0}{1}", search.Sort, search.ReverseSort ? " descending" : string.Empty));

            if (search.StartRecord.HasValue && search.StartRecord > 0)
            {
                query = query.Skip(search.StartRecord.Value);
            }

            query.Take(search.Count ?? 20);
            return query;
        }
    }
}
