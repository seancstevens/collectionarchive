﻿using CA.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class CategorizationLogic : BaseLogic
    {
        public IList<Categorization> GetDisabled()
        {
            var retVal = db.Categorizations.Where(item => item.IsActive == false);
            return retVal.ToList();
        }

        public IList<Categorization> GetByCollectionId(int id)
        {
            IList<Categorization> retVal = db.Categorizations
                .Include(i => i.CategorizationOptions)
                .Where(u => u.IsActive == true)
                .Where(u => u.AssetCollectionId == id)
                .ToList();

            return retVal;
        }
    }
}
