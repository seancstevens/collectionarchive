﻿using CA.DAL.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class AssetLogic : BaseLogic
    {
        public IList<Asset> GetDisabled()
        {
            var retVal = db.Assets.Where(item => item.IsActive == false);
            return retVal.ToList();
        }

        public IList<Asset> GetByCollectionId(int id)
        {
            var retVal = db.Assets
                .Include(i => i.TextFieldValues)
                .Include(i => i.CategorizationValues)
                .Where(i => i.AssetCollectionId == id && i.IsActive == true);

            return retVal.ToList();
        }

        public IList<Asset> GetDisabledByCollectionId(int id)
        {
            var retVal = db.Assets
                .Where(i => i.AssetCollectionId == id && i.IsActive == false);

            return retVal.ToList();
        }

        public Asset BulkPost(Asset asset)
        {
            asset.CreatedDate = DateTime.Now;
            asset.StartDate = DateTime.Now;
            db.Assets.Add(asset);
            db.SaveChanges();

            return asset;
        }

        public Asset BulkPut(Asset asset)
        {
            asset.ModifiedDate = DateTime.Now;
            db.Assets.Update(asset);
            db.SaveChanges();

            return asset;
        }
    }
}
