﻿using CA.DAL.Model;
using System.Collections.Generic;
using System.Linq;

namespace CA.BLL
{
    public class TextFieldLogic : BaseLogic
    {
        public IList<TextField> GetDisabled()
        {
            var retVal = db.TextFields.Where(item => item.IsActive == false);
            return retVal.ToList();
        }

        public IList<TextField> GetByCollectionId(int id)
        {
            IList<TextField> retVal = db.TextFields
                .Where(u => u.IsActive == true)
                .Where(u => u.AssetCollectionId == id)
                .ToList();

            return retVal;
        }
    }
}
